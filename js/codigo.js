console.log("Hello world estoy en codigo js")

/*
Data types:
undefined, null, number,string,boolean,objects
*/

// Scope en todo el programa
var myName = "Diego"
console.log(myName)
var myName = "Alejandro"
console.log(myName)

// Scope en el bloque
let myCity = "Bogota"

console.log(myCity)

// Constantes
const PI = 3.1416;
console.log(PI)
// Si cambio el valor de PI genera error

var a, b;
a = 3; // asignando constante
b = a //asignando a variable
c = a + b
console.log(a, b, c)

var sum = 10 + 10; //suma
sum++; // incrementar
sum += 4; //icrementra por un valor X
console.log(sum)

var subs = 10 - 10;
subs--;
subs -= 3;
console.log(subs)

var mult = 2 * 3
mult *= 2
console.log("Multiplicacion =", mult)

var divide = 10.5 / 3.7;
divide /= 2;
console.log(divide)

var remainder = 20 % 6
console.log(remainder);

//Cadenas de caracteres

var firstname = "Diego"

var lastname = "Azuero"

console.log(firstname, lastname);

var phrase1 = "este es el \t  con espacio caracter";
var phrase2 = "este es el \n caracter";
console.log(phrase1);
console.log(phrase2);

var phrase3 = `caracter=' y caracter="`;
console.log(phrase3);

var themovie = "Star"+ "Wars"
console.log(themovie);

themovie += ": The return of the Jedi"
console.log(themovie);

var year = 1983

themovie += "-" + year;
console.log(themovie);

//longitud 
var length = themovie.length
console.log(length);
//posicion
var letter =themovie[3];
console.log(letter);

//Arreglos

var elements =["Cata",28,"Cardozo"];
console.log(elements);

// acceder a un elemento

var element=elements[0]
console.log(element);
elements[1]=50
console.log(elements);

// agregar elemento al final
elements.push(200)
console.log(elements); 

// eliminar ultimo elemento
elements.pop()
console.log(elements);

//eliminar primer elemento
elements.shift()
console.log(elements);

//añadir elemento al inicio
elements.unshift("Medellin")
console.log(elements);

// copiar arreglo
var elements2 = [...elements]
console.log(elements2);
elements2.push("Colombia")
console.log(elements);
console.log(elements2);

var matrix=[
    ["A","B","C"],
    ["D","E","F"],
    ["G","H","I"],
]
console.log(matrix[1][1]);
matrix.push(["J","K","L"])
console.log(matrix);

//funciones 

//Definicion funcion
function myfunction(){
    console.log("This is my function");
}

//llamado a la funcion

myfunction()

// func con argumentos

function suma(num1, num2){

    return num1+num2
}

var result = suma(4,3)
console.log(result);

// valor que pide y si no,toma uno por defecto que yo asigne
function incrementvalue(value, increment=1){

return value+increment
}
console.log(incrementvalue(5, 2));
console.log(incrementvalue(10));

//var, let,const

var globalvariable =8;

// undefined como variable
function function1(){
     let localvariable2 =15; //tiene prioridad */
    if( localvariable2 === undefined){
        console.log("No Existe");
    }else{
        console.log("Existe");
    }
}
function1()

// tipo de dato
function function2(){
    if(typeof cualquiercosa === "undefined"){
        console.log("No Existe2");
    }else{
        console.log(" existe2");
    }
}

function2();


function agregar(lista,elemento){

    lista.push(elemento)
    elemento +=1;
}


let elementos =[1,2,3,4,5]
let num= 10;
console.log(num);
agregar(elementos,num)
console.log(elementos);

//arrow functions

function mensaje_clasico(){
console.log('clasico');

}

/* const mensaje_arrow = () => console.log('mensaje arrow');
mensaje_clasico()
mensaje_arrow()

const suma =(num1,num2)=> {
let resultado
resultado=num1+num2;
return resultado;
} */


let x=10

if (x==5){
console.log('La variable tiene un valor de 5');

}


if (x%2 == 0 ){

    console.log('El numero es par');
}
else{
    console.log('El numero es impar');
}

//multiples opciones

let valor =-5;

if(valor ==0){
    console.log('es cero');
}
else if(valor >0){
    console.log('es positivo');
}
else{

console.log('es negativa');

}

let dia =7;
switch(dia){

    case 1:
        console.log('lunes');
        break
        case 2:
            console.log('martes');
            break
            case 3:
        console.log('miercoles');
        break
        case 4:
        console.log('jueves');
        break
        case 5:
        console.log('viernes');
        break

    default:
        console.log('Fin de semana');
        break
}


let numero =3
let cadena ='3';

if(numero === cadena){
    console.log('son iguales');
}
else{
    console.log('son diferentes');
}

//ternarias

let edad= 18
let mensaje = edad >= 18 ? "adulto" : "niño"
console.log(mensaje);

//ciclos

var z=0;
while(z<5){
    console.log(z);
    z++;
}

let arregloprueba =[2,5,1,6];
let res=arregloprueba.join("=")
console.log(res);
res=arregloprueba.slice(1,3)
console.log(res);

res=arregloprueba.reduce((x,y)=>x+y);
console.log(res);

res=arregloprueba.map(f => f*2)
console.log(res);

res=arregloprueba.sort()
console.log(res);

/* arregloprueba.forEach((x)=> console.log(x));  */
arregloprueba.forEach((x)=>{
console.log(x);
console.log(x+1);
})

//objetos

let usuario={

    nombre:"Diego",
    edad:18,
    altura:1.78,
    notas: [1,2,3],
    ubicacion:{
        ciudad:"Bogota",
        pais:"Colombia"
    },
    canciones:[
        {nombre:"Help",artista:"The beatles"},
        {nombre:"One",artista:"Metalica"},
        {nombre:"xx",artista:"bad bunny"},
    ],
    cantidadnotas(){
        return this.notas.length
    }
}
console.log(usuario.notas)
console.log(usuario.cantidadnotas());
usuario.apellido="Azuero"
console.log(usuario);

delete usuario.apellido
console.log(usuario);